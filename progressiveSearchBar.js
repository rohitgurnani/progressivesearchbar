import {
    LightningElement,
    api,
    track
} from "lwc";

import searchByAccountName from "@salesforce/apex/AccountClass.searchByAccountName";

export default class ProgressiveSearchBar extends LightningElement {

    @api label='Account';
    @api inputPlaceholder = "Type to Search";
    @api isrequired = false;
    @api accounttype;

    isOpen = false;
    handlersAdded = false;
    hasFocus = false;
    selectedValue;

    @track accountList = [];
    @track isListEmpty;
    @track selectedAccount=null;
    @track showError = false;
    @api accountSelected;
    @api accountEvent;
    @track accountClass = "slds-input slds-combobox__input";
    @api parentAccountValue;
   
    connectedCallback() {
        console.log('In connnected', this.accountSelected);
        console.log('In connnected for event', this.accountEvent);
        console.log('In parent', this.parentAccountValue);
    }
    // dispatch SSA select Event
    dispatchAccountSelectEvent = _ => {
        if (this.selectedAccount) {
            this.template
                .querySelector(".input-container")
                .classList.add("input-hidden");
        } else {
            this.template
                .querySelector(".input-container")
                .classList.remove("input-hidden");
            this.template.querySelector("input").focus();
        }
        const ssaSelectEvent = new CustomEvent("accountselect", {
            detail: {
                selectedAccount: this.selectedAccount
            }
        });
        this.dispatchEvent(ssaSelectEvent);
    };
    // get RequiredAccounts List
    getAccountList = _ => {
        if (this.selectedAccount) {
            return;
        }
        
        let accountName = this.template.querySelector("input").value;
        if (accountName.trim().length <= 2) {
            return;
        }
        console.log('accountName--', accountName);
        console.log('parentAccountValue--', this.parentAccountValue);

        searchByAccountName({
                accountName: accountName,
                parentAccount: this.parentAccountValue
            })
            .then(response => {
                console.log('response-->', response);
                this.accountList = response;
                this.template
                    .querySelector(".slds-combobox")
                    .classList.add("slds-is-open");
                this.isOpen = true;
                this.isListEmpty = !(this.accountList.length > 0);
            })
            .catch(error => {
                console.log("Error: " + (error.message || error.body.message));
                /*showMessage(this, {
                  title: "Error",
                  message: error.message || error.body.message,
                  messageType: "error",
                  mode: "pester"
                });*/
            });
    };

    /**
     * Various UI Handlers for handling the Combobox hide and show events
     */

    selectAccount = event => {
        let id = event.currentTarget.dataset.id;

        this.accountList.some(account => {
            if (account.Id === id) {
                this.selectedAccount = account;
                this.selectedValue=account.Name;
                if (this.accountEvent) {
                    this.handleAccountSelected(account);
                }
                /*this.selectedAccountAddress = this.fetchAccountAddress(
                  this.selectAccount
                );*/
                this.template
                    .querySelector(".slds-combobox")
                    .classList.remove("slds-is-open");
                this.isOpen = false;
                this.dispatchAccountSelectEvent();
                this.showError = false;
                return true;
            }

            return false;
        });
        event.preventDefault();
    };

    @api
    validateInput() {
        console.log("isValid");
        if (this.isrequired) {
            if (this.selectedAccount == null) {
                this.accountClass =
                    "slds-input slds-combobox__input slds-has-error error";
                console.log("this.accountClass---", this.accountClass);
                this.showError = true;
                return false;
            } else {
                this.accountClass = "slds-input slds-combobox__input";
                console.log("this.accountClass---", this.accountClass);
                this.showError = false;
                return true;
            }
        } else {
            return true;
        }
    }

    deselectAccount = event => {
        this.selectedAccount = null;
        this.accountSelected = null;
        this.selectedValue=null;
        this.template.querySelector("input").value = "";
        this.getAccountList();
        console.log("isrequired---", this.isrequired);
        if (this.isrequired) {
            this.accountClass =
                "slds-input slds-combobox__input slds-has-error error";
            this.showError = true;
        } else {
            this.accountClass = "slds-input slds-combobox__input";
            this.showError = false;
        }
        this.dispatchAccountSelectEvent();
    };

    // handle key up event in input
    inputKeyUp = event => {
        let keyCode = event.keyCode;

        /*if (keyCode === 27) {
          this.template
            .querySelector(".slds-combobox")
            .classList.remove("slds-is-open");
          this.isOpen = false;
        } else if (keyCode === 13) {
          if (this.accountList.length > 0) {
            this.selectedAccount = this.accountList[0];
            this.template
              .querySelector(".slds-combobox")
              .classList.remove("slds-is-open");
            this.isOpen = false;
            this.dispatchAccountSelectEvent();
          }
        } else {
          this.getAccountList();
        }*/

        this.getAccountList();
    };
    // handle refresh event
    handleInputClicked = event => {
        event.preventDefault();
        this.getAccountList();
    };

    hideCombobox = event => {
        if (event.currentTarget === event.target) {
            this.template
                .querySelector(".slds-combobox")
                .classList.remove("slds-is-open");
        }
    };

    comboboxDropdownClicked = event => {
        event.preventDefault();
    };

    renderedCallback() {
        if (this.accountSelected != undefined || this.accountSelected != null) {
            this.selectedAccount = this.accountSelected;
            this.template
                .querySelector(".slds-combobox")
                .classList.remove("slds-is-open");
            this.isOpen = false;
            this.dispatchAccountSelectEvent();
            this.showError = false;
        }
        
        if (!this.handlersAdded) {
            this.template
                .querySelector(".slds-dropdown")
                .addEventListener("focusout", this.hideCombobox);
            this.template
                .querySelector(".slds-dropdown")
                .addEventListener("focusin", _ => {
                    this.hasFocus = true;
                });
            this.template.querySelector("input").addEventListener("blur", event => {
                if (this.isOpen) {
                    this.hasFocus = false;
                    setTimeout(_ => {
                        if (!this.hasFocus) {
                            this.template
                                .querySelector(".slds-combobox")
                                .classList.remove("slds-is-open");
                        }
                    }, 0);
                }
            });
            this.handlersAdded = true;
        }
    }

    handleAccountSelected(nameVal) {
        
        // Creates the event with the data.
        const selectedEvent = new CustomEvent("handleaccountselected", {
            detail: nameVal
        });

        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
}